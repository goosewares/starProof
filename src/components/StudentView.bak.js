'use strict';
import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
//import styles from "styles/screen.scss";
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import Theme from './theme';
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator';


var Select = require('react-select');
var log = require('loglevel');
var _ = require('lodash');
var options = {multi:true,className:"col-xs-12"};
var Geosuggest = require('react-geosuggest');
import DatePicker from 'material-ui/lib/date-picker/date-picker';
var moment = require('moment');

import AtomicForm from './form/formValidator'
import { connect } from 'react-redux'
import { pushState } from 'redux-simple-router'
import { loadSchools,loadCourses,loadCountries,loadStudent,saveStudent,removeIntendedCourse } from './actions'
import { bindActionCreators } from 'redux'
import { Date } from './form'
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';
import RaisedButton from 'material-ui/lib/flat-button';
import InvoiceView from './InvoiceView';

@connect(function(state){return {
            countries:state.entities.countries,
            student:state.entities.student,
            schools:state.entities.schools,
            courses:state.entities.courses
        }
    },
    {loadSchools,loadCourses,loadCountries,loadStudent,saveStudent,removeIntendedCourse} 
)
@ThemeDecorator(ThemeManager.getMuiTheme(Theme))
class StudentView extends React.Component{
    constructor(props){
        super(props)
        this.state = this.getState()
        this.submit = this.submit.bind(this);
        this.reloads = [];
        this.addIntendedCourse = this.addIntendedCourse.bind(this);
    }
    enableButton(){
        this.setState({canSubmit:true});
    }
    disableButton(){
        this.setState({canSubmit:false});
    }
    getState(){
        return{
            canSubmit:false,
            student: {},
            open: false
        }
    }
    handleOpen = () =>{
        this.setState({open:true});
    }
    handleClose = () =>{
        this.setState({open:false});
    }
    getCountries(data){
        var countries = [];
        for(var i in data){
            countries[i] = {value:data[i].id,label:data[i].name}
        }
        return countries;
    }
    mapper(content,callback){
        if(typeof content != 'undefined' && typeof content.map != undefined){
            return content.map(callback);
        }
    }
    addIntendedCourse(){
        this.state.student.intendedCourses.push({courseID:'',schoolID:''});
        this.setState({student:this.state.student});
    }
    componentWillMount(){
        var studentid = this.props.params.studentid;
        if(this.props.countries.length<=0){
            this.props.loadCountries();
        }
        if(this.props.schools.length<=0){
            this.props.loadSchools();
        }
        if(this.props.courses.length<=0){
            this.props.loadCourses();
        }
        if(typeof studentid != 'undefined'){
            if(typeof this.props.student[studentid] == 'undefined'){
                this.props.loadStudent(studentid);
            }else{
                this.setState({student: this.props.student[studentid]});
            }
        }else{
            this.setState({student:{citizenshipID:218,passportCountry:218}});
        }
    }
    componentDidMount(){
        this.refs.student.forceUpdate();
    }
    componentWillReceiveProps(nextProps){
        var studentid = nextProps.params.studentid;
        if(typeof studentid != 'undefined' && typeof nextProps.student[studentid] != 'undefined'){
            this.setState({
                student: nextProps.student[studentid]
            });
        }
    }
    reloadProps(){
        for(var i in this.reloads){
            var reload = this.reloads[i];
            reload();
        }
        this.reloads = [];
    }
    removeIntendedCourse(course){
        var i = this.state.student.intendedCourses.indexOf(course);
        this.state.student.intendedCourses.splice(i,1);
        this.setState(this.state);
        if(typeof course.id == 'undefined'){
            this.props.removeIntendedCourse(course.id);
        }
    }
    submit(model){
        if(this.props.params.studentid){
            model.id=this.props.params.studentid;
        }
        this.props.saveStudent(model);
        this.reloadProps();
    }
    checkInt(string){
        if(typeof string.match != 'undefined'){
            return string.match(/[0-9]/g)?true:false;
        }
    }
    selectChange(val){
        if(typeof val.gmaps =='undefined'){
            this.parent.refs.student.refs[this.name].value = val;
            if(!this.parent.checkInt(val)){
                if(this.parent.reloads.indexOf(this.reload)==-1){
                    this.parent.reloads.push(this.reload);
                }
            }
        }else{
            this.parent.refs.student.refs[this.name].value = val.label;
            var addressTypes= [{name:'StreetNumber',type:'street_number'},
                {name:'Sublocality',type:'sublocality'},
                {name:'Street',type:'route'},
                {name:'City',type:'locality'},
                {name:'State',type:'administrative_area'},
                {name:'Postcode',type:'postal_code'},
                {name:'Country',type:'country'}];
            for(var type in addressTypes){
                var aT = addressTypes[type];
                var re = new RegExp('^'+aT.type,'g');
                var content = _.filter(val.gmaps.address_components,function(o){return o.types[0].match(re);});
                if(aT.type=='country'){
                var name = _.find(this.parent.props.countries,{'name':content[0].long_name});
                if(typeof name.id != 'undefined'){
                    content=name.id;
                }
                }else{
                    content=_.map(content,'short_name').join(',');
                }
                this.parent.refs.student.refs[this.name+aT.name].value = content;
            }
        }

    }
    returnIf(input,item){
        if(typeof input != 'undefined'){
            if(typeof input[item] != 'undefined'){
                return input[item];
            }
        }
        return null;
    }
    render(){
        var parent = this;
        var student = this.state.student;
        window.reloads = this.reloads;
        var options = {onChange:this.selectChange};
        var myoptions = _.defaultsDeep({name:'documents',options:[
            {value:'Current Passport',label:'Current Passport'},
            {value:'Current Visa',label:'Current Visa'},
            {value:'Highest Qualification Transcript',label:'Highest Qualification Transcript'},
            {value:'Name Change Certificate',label:'Name Change Certificate'},
            {value:'Marriage Certificate',label:'Marriage Certificate'},
            {value:'Current COE',label:'Current COE'},
            {value:'Current OSHC',label:'Current OSHC'}
            ],value:this.returnIf(student,'documents'),
            onChange:function(val){console.log(this,val)}
            }
            ,options);
        var schools = this.getCountries(this.props.schools);
        var courses = this.getCountries(this.props.courses);
        var countries = this.getCountries(this.props.countries);
        var mycountries = _.defaultsDeep({options:countries,onChange:this.selectChange}
            ,options);
        var schoolList = _.defaultsDeep({multi:false,allowCreate:true,parent:this,options:schools,reload:this.props.loadSchools},options);
        var courseList = _.defaultsDeep({multi:false,allowCreate:true,parent:this,options:courses,reload:this.props.loadCourses},options);
        var citizenshipCountry = _.defaultsDeep({name:'citizenshipID',multi:false,allowCreate:true,value:this.returnIf(student,'citizenshipID'),parent:this},mycountries);
        var passportCountry = _.defaultsDeep({name:'passportCountry',multi:false,value:this.returnIf(student,'passportCountry'),parent:this},mycountries);
        
        const actions = [
            <FlatButton label="Cancel" secondary={true} onTouchTap={this.handleClose} />,
            <FlatButton label="Submit" primary={true} disabled={true} onTouchTap={this.handleClose} />
        ];
        return(
            <AtomicForm ref="student"  doSubmit={this.submit} initialData={this.state.student} onValid={this.enableButton} onInvalid={this.disableButton} id="student">
                <div className="row">
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="fullname" >Full Name</label>
                            <input className="col-xs-12" type="text" id="fullname" name="fullname" ref="fullname"/>
                        </div>
                        <div className="cols">
                            <label htmlFor="preferredName" className="col-xs-12">Preferred Name</label>
                            <input type="text" id="preferredName" name="preferredName" className="col-xs-12" ref="preferredName"/>
                        </div>
                    </div>
                    <RaisedButton label="Test Dialog" onTouchTap={this.handleOpen} />
                    <Dialog title="Invoice" actions={actions} modal={true} open={this.state.open} ><InvoiceView /></Dialog>
                    <div className="formgroup1">
                        {this.mapper(student.intendedCourses,function(course,i){
                            var theCourse = _.defaultsDeep({value:course.courseID,name:'intendedCourses.'+i+'.courseID'},courseList);
                            var theSchool = _.defaultsDeep({value:course.schoolID,name:'intendedCourses.'+i+'.schoolID'},schoolList);
                            var boundClick = parent.removeIntendedCourse.bind(parent,course);
                            return(
                               <div> <Select {...theSchool} /><Select {...theCourse} />
                                <input type="hidden" ref={"intendedCourses."+i+".schoolID"} name={"intendedCourses."+i+".schoolID"} />
                                <input type="hidden" ref={"intendedCourses."+i+".courseID"} name={"intendedCourses."+i+".courseID"} />
                                <div onClick={boundClick}>Remove</div>
                               </div>
                                );
                        })}
                        <div onClick={this.addIntendedCourse}>Add IntendedCourse</div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label htmlFor="dob" className="col-xs-12" >Date of birth</label>
                            <Date name="dob" value={this.returnIf(student,'dob')} onChange={this.selectChange} parent={this} />
                            <input ref ="dob" type="hidden" name="dob" id="dob" />
                        </div>
                        <div className="cols">
                            <label htmlFor="citizenship_id" className="col-xs-12" >Country of Citizenship</label>
                            <Select {...citizenshipCountry} />
                            <input type="hidden" ref="citizenshipID" name="citizenshipID" />
                        </div>
                    </div>
                    <div className="label" className="col-xs-12">Passport Details</div>

                    <div className="formgroup1">

                        <div className="cols">
                            <label className="col-xs-12" htmlFor="passportNumber" >Passport Number</label>
                            <input className="col-xs-12" type="text" id="passportNumber" name="passportNumber" ref="passportNumber"/>
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="passportCountry" >Country of Passport</label>
                            <Select {...passportCountry} />
                            <input type="hidden" ref="passportCountry" name="passportCountry" />
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="passportIssue" >Date of Issue</label>
                            <Date name="passportIssue" value={this.returnIf(student,'passportIssue')} onChange={this.selectChange} parent={this} />
                            <input type="hidden" ref="passportIssue" name="passportIssue" id="passportIssue" />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="passportExpire" >Date of Expiry</label>
                            <Date name="passportExpire" value={this.returnIf(student,'passportExpire')} parent={this} onChange={this.selectChange} />
                            <input type="hidden" ref="passportExpire" name="passportExpire"/>
                        </div>
                    </div>
                    <div className="formgroup1">


                        <div className="cols">
                            <label className="col-xs-12" htmlFor="visaExpire" >Visa Expiry</label>
                            <Date name="visaExpire" value={this.returnIf(student,'visaExpire')} parent={this} onChange={this.selectChange} />
                            <input type="hidden" ref="visaExpire" name="visaExpire"/>
                        </div>
                    </div>


                    <div className="label">Address in Australia</div>
                    <div className="formgroup1">
                        <div className="cols" >
                            <label className="col-xs-12" htmlFor="addressAustralia" >Address in Australia</label>
                            <Geosuggest onSuggestSelect={this.selectChange} parent={this} country="au" initialValue={this.returnIf(student,'addressAustralia')} name="addressAustralia" className="col-xs-12" />
                            <input ref="addressAustralia" name="addressAustralia" type="hidden" name="addressAustralia" id="addressAustralia" />
                            <input ref="addressAustraliaStreetNumber" name="addressAustraliaStreetNumber" type="hidden" name="addressAustraliaStreetNumber" id="addressAustraliaStreetNumber" />
                            <input ref="addressAustraliaStreet" name="addressAustraliaStreet" type="hidden" name="addressAustraliaStreet" id="addressAustraliaStreet" />
                            <input ref="addressAustraliaSublocality" name="addressAustraliaSublocality" type="hidden" name="addressAustraliaSublocality" id="addressAustraliaSublocality" />
                            <input ref="addressAustraliaCity" name="addressAustraliaCity" type="hidden" name="addressAustraliaCity" id="addressAustraliaCity" />
                            <input ref="addressAustraliaCountry" name="addressAustraliaCountry" type="hidden" name="addressAustraliaCountry" id="addressAustraliaCountry" />
                            <input ref="addressAustraliaState" name="addressAustraliaState" type="hidden" name="addressAustraliaState" id="addressAustraliaState" />
                            <input ref="addressAustraliaPostcode" name="addressAustraliaPostcode" type="hidden" name="addressAustraliaPostcode" id="addressAustraliaPostcode" />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="phoneAustralia" >Contact Number Australia</label>
                            <input className="col-xs-12" type="text" id="phoneAustralia" name="phoneAustralia" ref="phoneAustralia"/>
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="addressCountry" >Address in your country</label>
                            <Geosuggest onSuggestSelect={this.selectChange} parent={this} country="" initialValue={this.returnIf(student,'addressHome')} name="addressHome" className="col-xs-12" />
                            <input ref="addressHome" name="addressHome" type="hidden" name="addressHome" id="addressHome" />
                            <input ref="addressHomeStreetNumber" name="addressHomeStreetNumber" type="hidden" name="addressHomeStreetNumber" id="addressHomeStreetNumber" />
                            <input ref="addressHomeStreet" name="addressHomeStreet" type="hidden" name="addressHomeStreet" id="addressHomeStreet" />
                            <input ref="addressHomeSublocality" name="addressHomeSublocality" type="hidden" name="addressHomeSublocality" id="addressHomeSublocality" />
                            <input ref="addressHomeCity" name="addressHomeCity" type="hidden" name="addressHomeCity" id="addressHomeCity" />
                            <input ref="addressHomeCountry" name="addressHomeCountry" type="hidden" name="addressHomeCountry" id="addressHomeCountry" />
                            <input ref="addressHomeState" name="addressHomeState" type="hidden" name="addressHomeState" id="addressHomeState" />
                            <input ref="addressHomePostcode" name="addressHomePostcode" type="hidden" name="addressHomePostcode" id="addressHomePostcode" />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="phoneCountry" >Contact Number Your Country</label>
                            <input className="col-xs-12" type="text" id="phoneCountry" name="phoneCountry" ref="phoneCountry" />
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="emailAddress" >Email Address</label>
                            <input className="col-xs-12" type="text" id="emailAddress" name="emailAddress" ref="emailAddress" />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="intendedCourse" >Intended Courses</label>
                            <input className="col-xs-12" type="text" id="intendedCourse" name="intendedCourse" />
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="nameDependent" >Name of Dependent</label>
                            <input className="col-xs-12" type="text" id="nameDependent" name="nameDependent" ref='nameDependent' />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="dependentDOB" >Date of Birth</label>
                            <Date name="dependentDOB" value={this.returnIf(student,'dependentDOB')} onChange={this.selectChange} parent={this} />
                            <input type="hidden" ref="dependentDOB" name="dependentDOB"/>
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="includedVisa" >Included in visa application:</label>
                            <input className="col-xs-12" type="checkbox" id="includedVisa" name="includedVisa" ref="includedVisa" />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="USI" >USI</label>
                            <input className="col-xs-12" type="text" id="USI" name="USI" ref="USI"/>
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="documents" >Documents</label>
                            <Select {...myoptions} />
                            <input type="hidden" ref="documents" name="documents" />
                        </div>
                        <div className="cols">
                            <label className="col-xs-12" htmlFor="extraDetails" >Extra Details</label>
                            <textarea className="col-xs-12" id="extraDetails" name="extraDetails" ref="extraDetails"></textarea>
                        </div>
                    </div>
                    <div className="formgroup1">
                        <div className="cols">
                        </div>
                        <div className="cols">
                            <button name="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </AtomicForm>
        );
    }

};
StudentView.propTypes ={
    countries: PropTypes.object.isRequired,
    student: PropTypes.object.isRequired
//    loadCountries: PropTypes.func.isRequired
}
module.exports = StudentView;
