'use strict';

var injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();


import React from 'react'
import {render} from 'react-dom'
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import { Router, Route, browserHistory } from 'react-router'
import { syncReduxAndRouter, routeReducer } from 'redux-simple-router'
import reducers from './reducers'
import configureStore from './store/configureStore';

var log = require('loglevel');

const store = configureStore();
//const reducer = combineReducers(Object.assign({}, reducers
//            /*, {routing: routeReducer}*/
//))

var Student = require('./StudentView');
var StudentList = require('./StudentListView');

var App = React.createClass({
    render(){
        var parent = this;
        return(
            <div ref="content" id="content" className="container-fluid">{(()=>{
                if (!parent.props.children){ return(<StudentList/>)}else{return parent.props.children}
            })()}</div>
        )
    }
}); 

var Menu = require('./Menu');
render(React.createElement(Menu),document.getElementById('nav'));

window.App = render((
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" name="home" component={App}>
                <Route name="student" path="student" component={Student} />
                <Route name="studentedit" path="student/:studentid" component={Student}/>
            </Route>
        </Router>
    </Provider>
    ),document.getElementById('main')
);

