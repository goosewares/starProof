import React from 'react';
import template from './templates/test2.rjade';

module.exports = class Testing extends React.Component{
    render(){
        return template(this.props);
    }
}
