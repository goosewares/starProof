import { CALL_API, Schemas } from '../middleware/api'
import * as _ from 'lodash'


export const STUDENT_REQUEST = 'STUDENT_REQUEST'
export const STUDENT_SUCCESS = 'STUDENT_SUCCESS'
export const STUDENT_FAILURE = 'STUDENT_FAILURE'
export const INTENDEDCOURSEREMOVE_REQUEST = 'INTENDEDCOURSEREMOVE_REQUEST'
export const INTENDEDCOURSEREMOVE_SUCCESS = 'INTENDEDCOURSEREMOVE_SUCCESS'
export const INTENDEDCOURSEREMOVE_FAILURE = 'INTENDEDCOURSEREMOVE_FAILURE'
export const INVOICEITEMREMOVE_REQUEST = 'INVOICEITEMREMOVE_REQUEST'
export const INVOICEITEMREMOVE_SUCCESS = 'INVOICEITEMREMOVE_SUCCESS'
export const INVOICEITEMREMOVE_FAILURE = 'INVOICEITEMREMOVE_FAILURE'
// Fetches a single student from the API.
// Relies on the custom API middleware defined in ../middleware/api.js.

function fetchStudent(studentID) {
  return {
    [CALL_API]: {
      types: [ STUDENT_REQUEST, STUDENT_SUCCESS, STUDENT_FAILURE ],
      endpoint: `student/?id=${studentID}`,
      options: {"method":"get"},
      schema: Schemas.STUDENT
    }
  }
}
// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadStudent(studentID) {
  return (dispatch, getState) => {
    var student = getState().entities.student;
    student = student[studentID];
    if (student) {
      return null
    }

    return dispatch(fetchStudent(studentID))
  }
}

function sendStudent(model){
    return {
        [CALL_API]:{
            types: [ STUDENT_REQUEST,STUDENT_SUCCESS,STUDENT_FAILURE],
            endpoint: `student/`,
            schema: Schemas.STUDENT,
            options: {"method":"post"},
            body: model
        }
    }
}
// Inserts a user into the Student API
export function saveStudent(model){
   return(dispatch,getState)=>{
       return dispatch(sendStudent(model))
   }
}

export function removeIntendedCourse(id){
    return{
        [CALL_API]:{
            types:[INTENDEDCOURSEREMOVE_REQUEST,INTENDEDCOURSEREMOVE_SUCCESS,INTENDEDCOURSEREMOVE_FAILURE],
            endpoint: `intended_course_remove?id=${id}`,
            schema: Schemas.STUDENT
        }
    }
}
export function removeInvoiceItem(id){
    return{
        [CALL_API]:{
            types:[INVOICEITEMREMOVE_REQUEST,INVOICEITEMREMOVE_SUCCESS,INVOICEITEMREMOVE_FAILURE],
            endpoint: `invoice?id=${id}`,
            schema: Schemas.INVOICE,
            options: {"method":"delete"}
        }
    }
}
export const DELETEINVOICE_REQUEST = 'DELETEINVOICE_REQUEST'
export const DELETEINVOICE_SUCCESS = 'DELETEINVOICE_SUCCESS'
export const DELETEINVOICE_FAILURE = 'DELETEINVOICE_FAILURE'

function deleteInvoice(id){
    return {
        [CALL_API]:{
            types: [DELETEINVOICE_REQUEST, DELETEINVOICE_SUCCESS, DELETEINVOICE_FAILURE],
            endpoint: `invoice?id=${id}`,
            schema: Schemas.INVOICE,
            options: {"method":"delete"}
        }
    }
}
export const INVOICE_REQUEST = 'INVOICE_REQUEST'
export const INVOICE_SUCCESS = 'INVOICE_SUCCESS'
export const INVOICE_FAILURE = 'INVOICE_FAILURE'

function fetchInvoice(){
    return {
        [CALL_API]:{
            types: [INVOICE_REQUEST, INVOICE_SUCCESS, INVOICE_FAILURE],
            endpoint: 'invoice',
            schema: Schemas.INVOICE
        }
    }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadInvoice() {
  return (dispatch, getState) => {
    return dispatch(fetchInvoice())
  }
}
export const SAVEINVOICE_REQUEST = 'SAVEINVOICE_REQUEST'
export const SAVEINVOICE_SUCCESS = 'SAVEINVOICE_SUCCESS'
export const SAVEINVOICE_FAILURE = 'SAVEINVOICE_FAILURE'

function sendInvoice(model){
    return {
        [CALL_API]:{
            types: [SAVEINVOICE_REQUEST, SAVEINVOICE_SUCCESS, SAVEINVOICE_FAILURE],
            endpoint: 'invoice',
            schema: Schemas.INVOICE,
            options: {"method":"post"},
            body: model
        }
    }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function saveInvoice() {
  return (dispatch, getState) => {
    return dispatch(sendInvoice(model))
  }
}

export const STUDENTS_REQUEST = 'STUDENTS_REQUEST'
export const STUDENTS_SUCCESS = 'STUDENTS_SUCCESS'
export const STUDENTS_FAILURE = 'STUDENTS_FAILURE'

// Fetches a single student from the API.
// Relies on the custom API middleware defined in ../middleware/api.js.

function fetchStudents() {
  return {
    [CALL_API]: {
      types: [ STUDENTS_REQUEST, STUDENTS_SUCCESS, STUDENTS_FAILURE ],
      endpoint: `students/`,
      schema: Schemas.STUDENT_ARRAY
    }
  }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadStudents() {
  return (dispatch, getState) => {
    const students = getState().entities.students
    if (students>0) {
      return null
    }

    return dispatch(fetchStudents())
  }
}


export const COUNTRY_REQUEST = 'COUNTRY_REQUEST'
export const COUNTRY_SUCCESS = 'COUNTRY_SUCCESS'
export const COUNTRY_FAILURE = 'COUNTRY_FAILURE'

function fetchCountries(){
    return {
        [CALL_API]:{
            types: [COUNTRY_REQUEST, COUNTRY_SUCCESS, COUNTRY_FAILURE],
            endpoint: 'country',
            schema: Schemas.COUNTRY_ARRAY
        }
    }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadCountries() {
  return (dispatch, getState) => {
    const countries = getState().entities.countries
    if (countries.length>0) {
      return null
    }

    return dispatch(fetchCountries())
  }
}

export const SCHOOL_REQUEST = 'SCHOOL_REQUEST'
export const SCHOOL_SUCCESS = 'SCHOOL_SUCCESS'
export const SCHOOL_FAILURE = 'SCHOOL_FAILURE'

function fetchSchools(){
    return {
        [CALL_API]:{
            types: [SCHOOL_REQUEST, SCHOOL_SUCCESS, SCHOOL_FAILURE],
            endpoint: 'school',
            schema: Schemas.SCHOOL_ARRAY
        }
    }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadSchools() {
  return (dispatch, getState) => {
    const schools = getState().entities.schools
    if (schools.length>0) {
      return null
    }

    return dispatch(fetchSchools())
  }
}

export const COURSE_REQUEST = 'COURSE_REQUEST'
export const COURSE_SUCCESS = 'COURSE_SUCCESS'
export const COURSE_FAILURE = 'COURSE_FAILURE'

function fetchCourses(){
    return {
        [CALL_API]:{
            types: [COURSE_REQUEST, COURSE_SUCCESS, COURSE_FAILURE],
            endpoint: 'course',
            schema: Schemas.COURSE_ARRAY
        }
    }
}

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export function loadCourses() {
  return (dispatch, getState) => {
    const courses = getState().entities.courses
    if (courses.length>0) {
      return null
    }

    return dispatch(fetchCourses())
  }
}

export function addIntendedCourses(id){
    return (dispatch,getState)=>{
        const student = getState().entities.student[id];
        student.intendedCourses.push({courseID:'',schoolID:''});
    }
}
export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE'

// Resets the currently visible error message.

export function resetErrorMessage(){
    return{
        type: RESET_ERROR_MESSAGE
    }
}
