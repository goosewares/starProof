import {Schema,arrayOf,normalize} from 'normalizr'
import { camelizeKeys } from 'humps'
require('es6-promise').polyfill();
import 'isomorphic-fetch'
import * as _ from 'lodash'

const API_ROOT = 'http://localhost:9002/api/';


const studentSchema = new Schema('student',{
    idAttribute: 'id'
});

const studentsSchema = new Schema('students',{
    idAttribute: 'id'
});

const schoolSchema = new Schema('schools',{
    idAttribute: 'id'
});

const courseSchema = new Schema('courses',{
    idAttribute: 'id'
});

const invoiceSchema = new Schema('invoices',{
    idAttribute: 'id'
});

const countrySchema = new Schema('countries',{
    idAttribute: 'id'
});


export const Schemas = {
    STUDENT: studentSchema,
    STUDENT_ARRAY: arrayOf(studentsSchema),
    SCHOOL: schoolSchema,
    SCHOOL_ARRAY: arrayOf(schoolSchema),
    COURSE: courseSchema,
    COURSE_ARRAY: arrayOf(courseSchema),
    INVOICE: invoiceSchema,
    INVOICE_ARRAY: arrayOf(invoiceSchema),
    COUNTRY: countrySchema,
    COUNTRY_ARRAY: arrayOf(countrySchema)
}

const originalOptions = {
  credentials: 'cors',
  method: 'get',
//  headers: {
//    'Accept': 'application/json',
//    'Content-Type': 'application/json'
//  }
}

function callApi(endpoint, schema,data,options) {

  const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint
  if(typeof data !='undefined'){
    if(data.constructor.name == 'Object') var body = JSON.stringify(data);
  }else{
      var body='';
  }
  var options = (typeof options != 'undefined')? options:{};
  options = _.defaultsDeep(options,originalOptions);
  if(options.method!='get'){
      options.body = body;
  }
  return fetch(fullUrl,Object.assign({},options))
    .then(response =>
      response.json().then(json => ({ json, response }))
    ).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json)
      }
      if(json.success!=true){
          return Promise.reject(json);
      }
      const camelizedJson = camelizeKeys(json.data)
      return Object.assign({},
        normalize(camelizedJson, schema)
      )
    })
}

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = Symbol('Call API')

export default store => next => action => {
  const callAPI = action[CALL_API]
  if (typeof callAPI === 'undefined') {
    return next(action)
  }

  let { endpoint } = callAPI
  const { schema, types,body,options } = callAPI

  if (typeof endpoint === 'function') {
    endpoint = endpoint(store.getState())
  }

  if (typeof endpoint !== 'string') {
    throw new Error('Specify a string endpoint URL.')
  }
  if (!schema) {
    throw new Error('Specify one of the exported Schemas.')
  }
  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('Expected an array of three action types.')
  }
  if (!types.every(type => typeof type === 'string')) {
    throw new Error('Expected action types to be strings.')
  }

  function actionWith(data) {
    const finalAction = Object.assign({}, action, data)
    delete finalAction[CALL_API]
    return finalAction
  }

  const [ requestType, successType, failureType ] = types
  next(actionWith({ type: requestType }))
  
  return callApi(endpoint, schema,body,options).then(
    response => next(actionWith({
      response,
      type: successType
    })),
    error => next(actionWith({
      type: failureType,
      error: error.message || 'Something bad happened'
    }))
  )
}

