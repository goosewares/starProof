import React, {Component} from 'react'
class dater extends Component{
   constructor(props){
       super(props)
       this.dateChange = this.dateChange.bind(this);
   }
   dateChange(){
       var items = ['year','month','day'],
           ref = {},
           error=false;
       for(var item in items){
           item = items[item];
           ref[item] = this.refs[item].value;
           this.refs[item].className='';
       }
       if(ref['year'].length!=4||ref['year']<1900||ref['year']>3000){
           this.refs.year.className="error";
           error=true;
        }
       if(ref['day'].length<1||ref['day']<1||ref['day']>32){
           this.refs.day.className="error";
           error=true;
       }
       if(ref['month'].length<1||ref['month']<1||ref['month']>12){
           this.refs.month.className="error";
           error=true;
       }
       if(!error){
           this.props.onChange(this.refs.year.value+'-'+this.refs.month.value+'-'+this.refs.day.value);
       }
   }
   componentWillReceiveProps(nextProps){
        if(typeof nextProps.value != 'undefined' && nextProps.value != null){
            var date = nextProps.value.split('-');
            this.refs.year.value = date[0];
            this.refs.month.value = date[1];
            this.refs.day.value = date[2];
        }
   }
   render(){
       return(
            <div>
                <input placeholder="YYYY" type="text" size="4" ref="year" onBlur={this.dateChange} name={this.props.name+'_year'}/>
                <input placeholder="MM" size="2" type="text" ref="month" onBlur={this.dateChange} name={this.props.name+'_month'}/>
                <input placeholder="DD" size="2" type="text" ref="day" onBlur={this.dateChange} name={this.props.name+"_day"}/>
                <input type="hidden" ref="date" name={this.props.name} value={this.props.value} />
            </div>
        )
    }
}
module.exports = dater;
