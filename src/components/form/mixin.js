import React from 'react';
export var FormMixin = ComposedComponent => class extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            canSubmit:false
        }
    }
    enableButton(){
        this.setState({canSubmit:true});
    }
    disableButton(){
        this.setState({canSubmit:false});
    }
}
