'use strict';
import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
//import styles from "styles/screen.scss";
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import Theme from './theme';
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator';


var Select = require('react-select');
var log = require('loglevel');
var _ = require('lodash');
var options = {multi:true,className:"col-xs-12"};
var Geosuggest = require('react-geosuggest');
import DatePicker from 'material-ui/lib/date-picker/date-picker';
var moment = require('moment');
var SimpleTpl = require('./simpleTpl');
import AtomicForm from './form/formValidator'
import { connect } from 'react-redux'
import { pushState } from 'redux-simple-router'
import { loadSchools,loadCourses,loadCountries,loadStudent,saveStudent,removeIntendedCourse } from './actions'
import { bindActionCreators } from 'redux'
import { Date } from './form'
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';
import RaisedButton from 'material-ui/lib/flat-button';
import InvoiceView from './InvoiceView';
@connect(function(state){return {
            countries:state.entities.countries,
            student:state.entities.student,
            schools:state.entities.schools,
            courses:state.entities.courses
        }
    },
    {loadSchools,loadCourses,loadCountries,loadStudent,saveStudent,removeIntendedCourse} 
)
@ThemeDecorator(ThemeManager.getMuiTheme(Theme))
class StudentView extends React.Component{
    constructor(props){
        super(props)
        this.state = this.getState()
        this.submit = this.submit.bind(this);
        this.reloads = [];
        this.addIntendedCourse = this.addIntendedCourse.bind(this);
    }
    enableButton(){
        this.setState({canSubmit:true});
    }
    disableButton(){
        this.setState({canSubmit:false});
    }
    getState(){
        return{
            canSubmit:false,
            student: {},
            open: false
        }
    }
    handleOpen = () =>{
        this.setState({open:true});
    }
    handleClose = () =>{
        this.setState({open:false});
    }
    getCountries(data){
        var countries = [];
        for(var i in data){
            countries[i] = {value:data[i].id,label:data[i].name}
        }
        return countries;
    }
    mapper(content,callback){
        if(typeof content != 'undefined' && typeof content.map != undefined){
            return content.map(callback);
        }
    }
    addIntendedCourse(){
        this.state.student.intendedCourses.push({courseID:'',schoolID:''});
        this.setState({student:this.state.student});
    }
    componentWillMount(){
        var studentid = this.props.params.studentid;
        if(this.props.countries.length<=0){
            this.props.loadCountries();
        }
        if(this.props.schools.length<=0){
            this.props.loadSchools();
        }
        if(this.props.courses.length<=0){
            this.props.loadCourses();
        }
        if(typeof studentid != 'undefined'){
            if(typeof this.props.student[studentid] == 'undefined'){
                this.props.loadStudent(studentid);
            }else{
                this.setState({student: this.props.student[studentid]});
            }
        }else{
            this.setState({student:{citizenshipID:218,passportCountry:218}});
        }
    }
    componentDidMount(){
        this.refs.student.refs.student.forceUpdate();
    }
    componentWillReceiveProps(nextProps){
        var studentid = nextProps.params.studentid;
        if(typeof studentid != 'undefined' && typeof nextProps.student[studentid] != 'undefined'){
            this.setState({
                student: nextProps.student[studentid]
            });
        }
    }
    reloadProps(){
        for(var i in this.reloads){
            var reload = this.reloads[i];
            reload();
        }
        this.reloads = [];
    }
    removeIntendedCourse(course){
        var i = this.state.student.intendedCourses.indexOf(course);
        this.state.student.intendedCourses.splice(i,1);
        this.setState(this.state);
        if(typeof course.id == 'undefined'){
            this.props.removeIntendedCourse(course.id);
        }
    }
    submit(model){
        if(this.props.params.studentid){
            model.id=this.props.params.studentid;
        }
//        this.props.saveStudent(model);
        console.log(model);
        this.reloadProps();
    }
    checkInt(string){
        if(typeof string.match != 'undefined'){
            return string.match(/[0-9]/g)?true:false;
        }
    }
    selectChange(val){
        if(typeof val.gmaps =='undefined'){
            this.parent.refs.student.refs[this.name].value = val;
            if(!this.parent.checkInt(val)){
                if(this.parent.reloads.indexOf(this.reload)==-1){
                    this.parent.reloads.push(this.reload);
                }
            }
        }else{
            this.parent.refs.student.refs[this.name].value = val.label;
            var addressTypes= [{name:'StreetNumber',type:'street_number'},
                {name:'Sublocality',type:'sublocality'},
                {name:'Street',type:'route'},
                {name:'City',type:'locality'},
                {name:'State',type:'administrative_area'},
                {name:'Postcode',type:'postal_code'},
                {name:'Country',type:'country'}];
            for(var type in addressTypes){
                var aT = addressTypes[type];
                var re = new RegExp('^'+aT.type,'g');
                var content = _.filter(val.gmaps.address_components,function(o){return o.types[0].match(re);});
                if(aT.type=='country'){
                var name = _.find(this.parent.props.countries,{'name':content[0].long_name});
                if(typeof name.id != 'undefined'){
                    content=name.id;
                }
                }else{
                    content=_.map(content,'short_name').join(',');
                }
                this.parent.refs.student.refs[this.name+aT.name].value = content;
            }
        }

    }
    returnIf(input,item){
        if(typeof input != 'undefined'){
            if(typeof input[item] != 'undefined'){
                return input[item];
            }
        }
        return null;
    }
    render(){
        window.reloads = this.reloads;
        var options = {onChange:this.selectChange};
        var schools = this.getCountries(this.props.schools);
        var courses = this.getCountries(this.props.courses);
        var countries = this.getCountries(this.props.countries);
        var mycountries = _.defaultsDeep({options:countries,onChange:this.selectChange}
            ,options);
        var student = this.state.student;
        var schoolList = _.defaultsDeep({multi:false,allowCreate:true,parent:this,options:schools,reload:this.props.loadSchools},options);
        var courseList = _.defaultsDeep({multi:false,allowCreate:true,parent:this,options:courses,reload:this.props.loadCourses},options);
        var citizenshipCountry = _.defaultsDeep({name:'citizenshipID',multi:false,allowCreate:true,value:this.returnIf(student,'citizenshipID'),parent:this},mycountries);
        var passportCountry = _.defaultsDeep({name:'passportCountry',multi:false,value:this.returnIf(student,'passportCountry'),parent:this},mycountries);
        console.log(SimpleTpl);
        var tplProps = {
            parent:this,
            AtomicForm: AtomicForm,
            Date: Date,
            RaisedButton:RaisedButton,
            Select:Select,
            Dialog:Dialog,
            Geosuggest:Geosuggest,
            InvoiceView:InvoiceView,
            Templator:SimpleTpl,
            state:this.state,
            submit: this.submit,
            myoptions:_.defaultsDeep({name:'documents',options:[
                {value:'Current Passport',label:'Current Passport'},
                {value:'Current Visa',label:'Current Visa'},
                {value:'Highest Qualification Transcript',label:'Highest Qualification Transcript'},
                {value:'Name Change Certificate',label:'Name Change Certificate'},
                {value:'Marriage Certificate',label:'Marriage Certificate'},
                {value:'Current COE',label:'Current COE'},
                {value:'Current OSHC',label:'Current OSHC'}
                ],value:this.returnIf(student,'documents'),
                onChange:function(val){console.log(this,val)}
            }
            ,options),
            actions:[
                <FlatButton label="Cancel" secondary={true} onTouchTap={this.handleClose} />,
                <FlatButton label="Submit" primary={true} disabled={true} onTouchTap={this.handleClose} />
            ],
            returnIf: this.returnIf,
            schools:schools,
            courses: courses,
            countries: countries,
            mycountries: mycountries,
            schoolList: schoolList,
            courseList: courseList,
            citizenshipCountry: citizenshipCountry,
            passportCountry: passportCountry,
        }
        console.log(tplProps);
        window.App = this;
        return(
            <SimpleTpl ref="student" jadeTpl='./templates/studentForm.rjade' tplProps={tplProps} />
        );
    }

};
StudentView.propTypes ={
    countries: PropTypes.object.isRequired,
    student: PropTypes.object.isRequired
//    loadCountries: PropTypes.func.isRequired
}
module.exports = StudentView;
