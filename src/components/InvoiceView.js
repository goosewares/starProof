'use strict';
import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';
//import styles from "styles/screen.scss";
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import Theme from './theme';
import ThemeDecorator from 'material-ui/lib/styles/theme-decorator';

var Select = require('react-select');
var log = require('loglevel');
var _ = require('lodash');
var options = {multi:true,className:"col-xs-12"};
var moment = require('moment');

import AtomicForm from './form/formValidator'
import { connect } from 'react-redux'
import { pushState } from 'redux-simple-router'
import { loadSchools,loadCourses,loadInvoice,saveInvoice } from './actions'
import { bindActionCreators } from 'redux'
import { Date } from './form'

@connect(function(state){return {schools:state.entities.schools,courses:state.entities.courses,invoice:state.entities.invoice}},{loadSchools,loadInvoice,saveInvoice} )
@ThemeDecorator(ThemeManager.getMuiTheme(Theme))
class StudentView extends React.Component{
    constructor(props){
        super(props)
        this.state = this.getState()
        this.submit = this.submit.bind(this);
    }
    enableButton(){
        this.setState({canSubmit:true});
    }
    disableButton(){
        this.setState({canSubmit:false});
    }
    getState(){
        return{
            canSubmit:false,
            invoice:{items:[]}
        }
    }
    prepareOptions(data){
        var options = [];
        for(var i in data){
            options[i] = {value:data[i].id,label:data[i].name}
        }
        return options;
    }
    mapper(content,callback){
        if(typeof content != 'undefined' && typeof content.map != undefined){
            return content.map(callback);
        }
    }
    componentWillMount(){
        var invoiceid = (typeof this.props.params != 'undefined')?this.props.params.invoiceid:this.props.invoiceid;
//        var invoiceid = (typeof this.props.params.invoiceid !='undefined')?this.props.params.invoiceid:this.props.invoiceid;

        if(this.props.schools.length<=0){
            this.props.loadCountries();
        }
        if(this.props.courses.length<=0){
            this.props.loadCourses();
        }
        if(typeof invoiceid != 'undefined'){
            if(typeof this.props.invoice[invoiceid] == 'undefined'){
                this.props.loadInvoice(invoiceid);
            }else{
                this.setState({student: this.props.invoice[invoiceid]});
            }
        }else{
            this.setState({student:{citizenship:218,passportCountry:218}});
        }
    }
    componentDidMount(){
        this.refs.student.forceUpdate();
    }
    componentWillReceiveProps(nextProps){
        var studentid = nextProps.params.studentid;
        console.log("i totally recieved some props");
        if(typeof studentid != 'undefined'){
            this.setState({
                student: nextProps.student[studentid]
            });
        }
    }
    submit(model){
        if(this.props.params.studentid){
            model.id=this.props.params.studentid;
        }
        this.props.saveStudent(model);
    }
    selectChange(val){
        if(typeof val.gmaps =='undefined'){
            this.parent.refs.student.refs[this.name].value = val;
        }else{
            this.parent.refs.student.refs[this.name].value = val.label;
            var addressTypes= [{name:'StreetNumber',type:'street_number'},
                {name:'Sublocality',type:'sublocality'},
                {name:'Street',type:'route'},
                {name:'City',type:'locality'},
                {name:'State',type:'administrative_area'},
                {name:'Postcode',type:'postal_code'},
                {name:'Country',type:'country'}];
            for(var type in addressTypes){
                var aT = addressTypes[type];
                var re = new RegExp('^'+aT.type,'g');
                var content = _.filter(val.gmaps.address_components,function(o){return o.types[0].match(re);});
                if(aT.type=='country'){
                var name = _.find(this.parent.props.countries,{'name':content[0].long_name});
                if(typeof name.id != 'undefined'){
                    content=name.id;
                }
                }else{
                    content=_.map(content,'short_name').join(',');
                }
                this.parent.refs.student.refs[this.name+aT.name].value = content;
            }
        }

    }
    returnIf(input,item){
        if(typeof input != 'undefined'){
            if(typeof input[item] != 'undefined'){
                return input[item];
            }
        }
        return null;
    }
    render(){
        var parent = this;
        var invoice = this.state.invoice;
        var schools = this.prepareOptions(this.props.schools);
        var courses = this.prepareOptions(this.props.courses);
        var schoolList = _.defaultsDeep({multi:false,allowCreate:true,parent:this,options:schools,reload:this.props.loadSchools},options);
        var courseList = _.defaultsDeep({multi:false,allowCreate:true,parent:this,options:courses,reload:this.props.loadCourses},options);
       return(
            <AtomicForm ref="student"  doSubmit={this.submit} initialData={invoice} onValid={this.enableButton} onInvalid={this.disableButton} id="student">
                <div className="row">
                {this.mapper(invoice.items,function(item,i){
                    var theCourse = _.defaultsDeep({value:course.courseID,name:'items.'+i+'.courseID'},courseList);
                    var theSchool = _.defaultsDeep({value:course.schoolID,name:'items.'+i+'.schoolID'},schoolList);
 
                    return(
                    <div className="formgroup2">
                         <div className="3cols">
                            <label htmlFor="school" className="col-xs-12">School</label>
                            <Select {...theSchool} />
                            <input type="hidden" id="school" name="school" className="col-xs-12" ref={'items.'+i+'.school'}/>
                        </div>
                        <div className="3cols">
                            <label className="col-xs-12" htmlFor="course" >Course</label>
                            <Select {...theCourse} />
                            <input className="col-xs-12" type="hidden" id="course" name="course" ref={'items.'+i+'.school'}/>
                        </div>
                         <div className="3cols">
                            <label htmlFor="amount" className="col-xs-12">Amount</label>
                            <input type="text" id="amount" name="amount" className="col-xs-12" ref={'items.'+i+'.amount'}/>
                        </div>
                   </div>
                   );
                })}
                   <div className="formgroup2">

                   </div>
                   <div className="formgroup1">
                        <div className="cols">
                        </div>
                        <div className="cols">
                            <button name="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </AtomicForm>
        );
    }

};
StudentView.propTypes ={
    countries: PropTypes.object.isRequired,
    student: PropTypes.object.isRequired
//    loadCountries: PropTypes.func.isRequired
}
module.exports = StudentView;
