'use strict';
import { render } from 'react-dom'
import React, { Component, PropTypes } from 'react';var Select = require('react-select');
require('react-datagrid/index.css')
var log = require('loglevel');
var _ = require('lodash');
var DataGrid = require('react-datagrid');

import { connect } from 'react-redux'
import { pushState } from 'redux-simple-router'
import { loadStudents } from './actions'
import { bindActionCreators } from 'redux'
import { Date } from './form'
import { Link } from 'react-router'
var Test = require("./simpleTpl");
var columns = [
    {name: 'fullname'},
    {name: 'preferredName'},
    {name: 'phoneAustralia'},
    {name: 'phoneCountry'},
    {name: 'id', title: 'Edit', render: function(v){return (<Link to={`/student/${v}`}>Edits</Link>  )}}
]

@connect(function(state){return {students:state.entities.students}},{loadStudents} )
class UserListView extends Component{
    componentWillMount(){
        if(this.props.students<1){
            this.props.loadStudents();
        }
    }
    toArray(items){
        var array = [];
        for(var item in items){
            array.push(items[item]);
        }
        return array;
    }
    render(){
        var tplProps = {Test2:Test,tests:{tplProps:{'tstr':'testing',testing:"Live updates work a treat !!!",tests:'moopsy'},jadeTpl:'./templates/test2.rjade'},testing:'Yes it works !!!'};
        
        return (
            <div>
                <input type="text" className="search" id="usersearch" name="usersearch" placeholder="Search User...." />
                <DataGrid
                    idProperty='id'
                    dataSource={/**/this.toArray(/**/this.props.students/**/)/**/}
                    columns={columns}
                />
            </div>
        );
    }
};
module.exports = UserListView;
